#!/bin/sh
# use CentOS-6 local mirror
# curl https://gitlab.pasteur.fr/tru/centos-pasteur/raw/master/c6-local-mirror.sh | sudo /bin/sh
#
/bin/rm /etc/yum.repos.d/CentOS-*.repo
wget https://gitlab.pasteur.fr/tru/centos-pasteur/raw/master/c6.repo -O /etc/yum.repos.d/local.repo

