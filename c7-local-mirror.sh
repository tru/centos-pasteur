#!/bin/sh
# use CentOS-7 local mirror
# curl https://gitlab.pasteur.fr/tru/centos-pasteur/raw/master/c7-local-mirror.sh | sudo /bin/sh
#
/bin/rm /etc/yum.repos.d/CentOS-*.repo
wget https://gitlab.pasteur.fr/tru/centos-pasteur/raw/master/c7.repo -O /etc/yum.repos.d/local.repo

