#!/bin/sh
# use gitlab-ci-multi-runner for CentOS-7 local mirror
# curl https://gitlab.pasteur.fr/tru/centos-pasteur/raw/master/local-c7-gitlab-ci-multi-runner.sh  | sudo /bin/sh
#
/bin/rm /etc/yum.repos.d/runner_gitlab-ci-multi-runner.repo
wget https://gitlab.pasteur.fr/tru/centos-pasteur/raw/master/local-c7-gitlab-ci-multi-runner.repo -O /etc/yum.repos.d/local-c7-gitlab-ci-multi-runner.repo

